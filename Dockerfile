FROM ubuntu:18.04

LABEL Yinjie Lee "liyinjie0418@gmail.com"

ENV TZ=Asia/Shanghai \
    DEBIAN_FRONTEND=noninteractive \
    GOVERSION=go1.18

RUN apt-get update \
    && apt update && apt install -y proxychains4 \
    && sed -i '$ d' /etc/proxychains4.conf \
    && sed -i '$ d' /etc/proxychains4.conf \
    && echo "socks5  192.168.1.51 7890" >> /etc/proxychains4.conf \
    && apt install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-toolchain-r/test && apt update \
    && apt install -y python git gettext-base \
    && proxychains4 apt install -y --no-install-recommends g++-10 make curl vim wget sqlite3 tree protobuf-compiler \
    && ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
    && echo ${TZ} > /etc/timezone \
    && update-alternatives --install  /usr/bin/gcc gcc  /usr/bin/gcc-10 10 \
    && update-alternatives --install  /usr/bin/g++ g++  /usr/bin/g++-10 10 \
    && update-alternatives --install  /usr/bin/cc cc  /usr/bin/gcc-10 10 \
    && update-alternatives --install  /usr/bin/cpp cpp  /usr/bin/cpp-10 10 \
    && update-alternatives --install  /usr/bin/gcov gcov  /usr/bin/gcov-10 10 \
    && update-alternatives --install  /usr/bin/gcov-dump gcov-dump  /usr/bin/gcov-dump-10 10 \
    && update-alternatives --install  /usr/bin/gcov-tool gcov-tool  /usr/bin/gcov-tool-10 10 \
    && update-alternatives --install  /usr/bin/gcc-ar gcc-ar  /usr/bin/gcc-ar-10 10 \
    && update-alternatives --install  /usr/bin/gcc-nm gcc-nm  /usr/bin/gcc-nm-10 10 \
    && update-alternatives --install  /usr/bin/gcc-ranlib gcc-ranlib  /usr/bin/gcc-ranlib-10 10 \
    && proxychains4 apt-get install -y --no-install-recommends python3-pip doxygen psmisc gdb python3-setuptools ninja-build valgrind rsync\
    && curl -sL https://deb.nodesource.com/setup_14.x | /bin/bash \
    && apt-get update \
    && proxychains4 apt-get install -y --no-install-recommends nodejs \
    && proxychains4 pip3 install jinja2 \
    && proxychains4 pip3 install 'cpplint == 1.5.5' \
    && proxychains4 pip3 install 'gcovr == 5.0' \
    && proxychains4 pip3 install conan \
    && npm install -g @commitlint/cli @commitlint/config-conventional \
    && apt-get autoremove && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && proxychains4 wget https://dl.google.com/go/${GOVERSION}.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf ${GOVERSION}.linux-amd64.tar.gz \
    && rm ${GOVERSION}.linux-amd64.tar.gz \
    && proxychains4 wget https://dl.google.com/go/${GOVERSION}.linux-arm64.tar.gz \
    && tar -xzf ${GOVERSION}.linux-arm64.tar.gz \
    && cp -R go/pkg/linux_arm64 /usr/local/go/pkg/ \
    && rm -fr go && rm -frv ${GOVERSION}.linux-arm64.tar.gz \
    && rm -rf /usr/local/bin/go \
    && rm -rf /usr/local/bin/gofmt

# Copy resources into docker
COPY root/.bashrc /root/.bashrc
COPY toolchains/cmake-3.24.0-linux-x86_64.tar.gz /root/

RUN conan config install https://gitlab.com/oppl2/conan_config.git --args="-b 1.0.4" \
    && conan user -p hoqdes-tozcy8-nujqaV -r zksd zksd \
    && tar zxvf /root/cmake-3.24.0-linux-x86_64.tar.gz -C /root/ \
    && rm -rf /usr/share/vim \
    && cp -rf /root/cmake-3.24.0-linux-x86_64/* /usr/ \
    && rm -rf /root/cmake-3.24.0-linux-x86_64* \
    && rm -rf /root/.conan/data/

# Configure Golang
ENV GOPATH=/root/workspace/apps/go
ENV GOROOT=/usr/local/go
ENV GOTOOLS=/usr/local/go/pkg/tool
ENV PATH=${PATH}:${GOPATH}/bin:/usr/local/go/bin

WORKDIR /root/workspace/apps

