<!--
 * @Author: Yinjie Lee
 * @Date: 2022-04-17 22:16:39
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-22 12:00:51
-->
# opdocker

## 简介

opdocker基于docker，打包了各平台[x64]基本的C/C++开发工具链，conan相关工具链，CI/CD工具链等，实现一键化的开发环境。

已支持的平台【持续更新中】
- Ubuntu x64

## 使用方法

安装opdocker

```bash

sudo rm -rf /usr/local/bin/opdocker && sudo wget -P /usr/local/bin/ https://oppl2.gitlab.io/opdocker/opdocker && sudo chmod a+x /usr/local/bin/opdocker
```

进入opdocker开发环境，只需要输入如下命令：

```bash
cd ${your_project_dir}

opdocker
```

编译一个example

```bash
cd /root/examples/
./build.sh -t x64
```

opdocker的参数介绍

```shell
opdocker                   # 进入docker进行编译，默认执行/bin/bash
opdocker <custom command>  # 用户自定义进入opdocker执行的命令，例如opdocker ls
opdocker -v                # 展示opdocker的版本号
opdocker -s ver            # 可以更新到制定版本，比如，opdocker -s 0.0.1
opdocker -c                # 会清理 除当前使用docker外的其他docker，包括container，image
opdocker -c all            # 会清除所有包括opdocker和当前使用的docker container和image
opdocker -h                # 显示opdocker的用法
```

Enjoy :)

